import React, { Component } from 'react'

import Dropzone from 'react-dropzone'
import ReactCrop from 'react-image-crop'
import './custom-image-crop.css';
import './ImgDropAndCrop.css';
import _ from 'lodash';
import {
    extractImageFileExtensionFromBase64,
    image64toCanvasRef} from './ResuableUtils'
import Axios from 'axios';
import {Form,Input,Button} from 'semantic-ui-react'

const imageMaxSize = 1000000000 // bytes
const acceptedFileTypes = 'image/x-png, image/png, image/jpg, image/jpeg, image/gif'
const acceptedFileTypesArray = acceptedFileTypes.split(",").map((item) => {return item.trim()})
class ImgDropAndCrop extends Component {
    constructor(props){
        super(props)
        this.imagePreviewCanvasRef = React.createRef()
        this.fileInputRef = React.createRef()
        this.state = {
            imgSrc: null,
            imgSrcExt: null,
            crop: {
                aspect: 1/1
            }
        }
    }

    verifyFile = (files) => {
        if (files && files.length > 0){
            const currentFile = files[0]
            const currentFileType = currentFile.type
            const currentFileSize = currentFile.size
            if(currentFileSize > imageMaxSize) {
                alert("This file is not allowed. " + currentFileSize + " bytes is too large")
                return false
            }
            if (!acceptedFileTypesArray.includes(currentFileType)){
                alert("This file is not allowed. Only images are allowed.")
                return false
            }
            return true
        }
    }

    handleOnDrop = (files, rejectedFiles) => {
        if (rejectedFiles && rejectedFiles.length > 0){
            this.verifyFile(rejectedFiles)
        }
        if (files && files.length > 0){
             const isVerified = this.verifyFile(files)
             if (isVerified){
                 // imageBase64Data 
                 const currentFile = files[0]
                 const myFileItemReader = new FileReader()
                 myFileItemReader.addEventListener("load", ()=>{
                     const myResult = myFileItemReader.result
                     this.setState({
                         imgSrc: myResult,
                         imgSrcExt: extractImageFileExtensionFromBase64(myResult)
                     })
                 }, false)
                 myFileItemReader.readAsDataURL(currentFile)
             }
        }
    }

    handleOnCropChange = (crop) => {
        this.setState({crop:crop})
    }
    
    handleOnCropComplete = (crop, pixelCrop) =>{
        const canvasRef = this.imagePreviewCanvasRef.current
        const {imgSrc}  = this.state
        image64toCanvasRef(canvasRef, imgSrc, pixelCrop)
    }

    handlePartDetectClick = (event) => {
        event.preventDefault()
        const imgSrc = this.state.imgSrc
        if (imgSrc) {
            const canvasRef = this.imagePreviewCanvasRef.current
            console.log('ref: ',canvasRef);
            const {imgSrcExt} =  this.state
            const imageData64 = canvasRef.toDataURL('image/' + imgSrcExt)
            // console.log('imageData64: ',imageData64);
            const formar_imgSrcs =  _.replace(imageData64,'data:image/jpeg;base64,', '')
             console.log(formar_imgSrcs)      
             //We use Axios to make a POST to REST
            Axios.post('http://127.0.0.1:5000/api/face', {"image": formar_imgSrcs})
            .then(response => {
                console.log('return image',response.data.data.image);
                 this.setState({imgSrc:response.data.data.image})
            })
            this.handleClearToDefault()
        }
    }

    handleClearToDefault = event =>{
        if (event) event.preventDefault()
        const canvas = this.imagePreviewCanvasRef.current
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        this.setState({
            imgSrc: null,
            imgSrcExt: null,
            crop: {
                aspect: 1/1
            }
        })
        this.fileInputRef.current.value = null
    }

    handleFileSelect = event => {
        const files = event.target.files
        if (files && files.length > 0){
              const isVerified = this.verifyFile(files)
             if (isVerified){
                 // imageBase64Data 
                 const currentFile = files[0]
                 const myFileItemReader = new FileReader()
                 myFileItemReader.addEventListener("load", ()=>{
                     // console.log(myFileItemReader.result)
                     const myResult = myFileItemReader.result
                     this.setState({
                         imgSrc: myResult,
                         imgSrcExt: extractImageFileExtensionFromBase64(myResult)
                     })
                 }, false)

                 myFileItemReader.readAsDataURL(currentFile)

             }
        }
    }
    handleSubmit= event =>{
        event.preventDefault()
        const  imgSrc = this.state.imgSrc
            const formar_imgSrc =  _.replace(imgSrc,'data:image/jpeg;base64,', '')
             console.log(formar_imgSrc)      
             //We use Axios to make a POST to REST
            Axios.post('http://127.0.0.1:5000/api/face', {"image": formar_imgSrc})
            .then(response => {
                console.log('return image',response.data.message);
                this.setState({imgSrc:response.data.data.image})
                const  imgSrc = this.state
                //console.log("ImageSrc",imgSrc)                
            })
    }



  render () {
      const {imgSrc} = this.state
    return (
      
        <div className="ui segment">
        <h2 className="ui right floated header">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQg7lU2ZnxsWe0bLMehBbttfjunVp1AZDHuPAkIkAADH3X0kFEO&usqp=CAU" 
        className="ui circular image" />
            Face Dtection</h2>
        <div className="ui clearing divider"></div>
        <Form onSubmit={this.handleSubmit}>
        <div className="ui inverted segment">
        <div className="ui inverted input">
        <div className="ui left corner labeled input">
        <div className="ui label label left corner"><i aria-hidden="true" className="asterisk icon"></i></div>
        <input ref={this.fileInputRef} type='file' accept={acceptedFileTypes} multiple={false} onChange={this.handleFileSelect} />
        </div>
        </div>
        </div>
        <Button className="ui black button">Detect</Button>
        </Form>
        {imgSrc !== null ? 
            <div>
                 <ReactCrop 
                src={imgSrc}
                crop={this.state.crop} 
                     onImageLoaded={this.handleImageLoaded}
                     onComplete = {this.handleOnCropComplete}
                     onChange={this.handleOnCropChange}/>
                  <br/>
                  <div class="ui orange message">Can Crop to Detect Face</div>
                  <canvas ref={this.imagePreviewCanvasRef}></canvas>
                  <Button onClick={this.handlePartDetectClick} className="ui grey button">Detect</Button>
                  <Button onClick={this.handleClearToDefault} className="ui brown button">Clear</Button>
              </div>
           : 
             <Dropzone onDrop={this.handleOnDrop} accept={acceptedFileTypes} multiple={false} maxSize={imageMaxSize}>Drop image here or click to upload</Dropzone>
         }  
         
          
      </div>
     
    
     
    
    )
  }
}

export default ImgDropAndCrop
